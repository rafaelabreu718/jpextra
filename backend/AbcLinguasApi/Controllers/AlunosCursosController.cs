﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AbcLinguasApi.Dados;
using AbcLinguasApi.Models;

namespace AbcLinguasApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlunosCursosController : ControllerBase
    {
        private readonly AbcLinguasContext _context;

        public AlunosCursosController(AbcLinguasContext context)
        {
            _context = context;
        }

        // GET: api/AlunosCursos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AlunoCurso>>> GetAlunoCurso()
        {
            return await _context.AlunoCurso.ToListAsync();
        }

        // GET: api/AlunosCursos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AlunoCurso>> GetAlunoCurso(int id)
        {
            var alunoCurso = await _context.AlunoCurso.FindAsync(id);

            if (alunoCurso == null)
            {
                return NotFound();
            }

            return alunoCurso;
        }

        // PUT: api/AlunosCursos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAlunoCurso(int id, AlunoCurso alunoCurso)
        {
            if (id != alunoCurso.AlunoId)
            {
                return BadRequest();
            }

            _context.Entry(alunoCurso).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlunoCursoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AlunosCursos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AlunoCurso>> PostAlunoCurso(AlunoCurso alunoCurso)
        {
            _context.AlunoCurso.Add(alunoCurso);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AlunoCursoExists(alunoCurso.AlunoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAlunoCurso", new { id = alunoCurso.AlunoId }, alunoCurso);
        }

        // DELETE: api/AlunosCursos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AlunoCurso>> DeleteAlunoCurso(int id)
        {
            var alunoCurso = await _context.AlunoCurso.FindAsync(id);
            if (alunoCurso == null)
            {
                return NotFound();
            }

            _context.AlunoCurso.Remove(alunoCurso);
            await _context.SaveChangesAsync();

            return alunoCurso;
        }

        private bool AlunoCursoExists(int id)
        {
            return _context.AlunoCurso.Any(e => e.AlunoId == id);
        }
    }
}
