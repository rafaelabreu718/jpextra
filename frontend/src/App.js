import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Home } from './components/Home';
import {Alunos} from './components/Alunos';
import {Professor} from './components/Professor';
import {Curso} from './components/Curso';

function App() {
  return (
    <Container className="Container">
      <BrowserRouter>
        <Navbar bg='light' expand='lg'>
          <Navbar.Brand as={Link} to="/">Escola Municpal Carvalho</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='mr-auto'>
             
              <Nav.Link as={Link} to="/Professor">Professor</Nav.Link>
              <Nav.Link as={Link} to="/alunos">Alunos</Nav.Link>
              <Nav.Link as={Link} to="/Curso">Curso</Nav.Link>
                          </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/alunos" component={Alunos} />
          <Route path="/Professor" component={Professor} />
          <Route path="/Curso" component={Curso} />
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default App;
