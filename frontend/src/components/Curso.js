import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';

export class Curso extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      curso: [],
      modalAberta: false
    };

    this.buscarCurso = this.buscarCurso.bind(this);
    this.buscarCurso = this.buscarCurso.bind(this);
    this.inserirCurso = this.inserirCurso.bind(this);
    this.atualizarCurso = this.atualizarCurso.bind(this);
    this.excluirCurso = this.excluirCurso.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarCurso();
  }

  // GET (todos Curso)
  buscarCurso() {
    fetch('https://localhost:44362/api/curso')
      .then(response => response.json())
      .then(data => this.setState({ curso: data }));
  }
  
  //GET (Curso com determinado id)
  buscarCurso(id) {
    fetch('https://localhost:44362/api/curso/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirCurso = (curso) => {
    fetch('https://localhost:44362/api/curso', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(Curso)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarCurso();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarCurso(curso) {
    fetch('https://localhost:44362/api/Curso/' + curso.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(curso)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarCurso();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirCurso = (id) => {
    fetch('https://localhost:44362/api/Curso/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarCurso();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarCurso(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const curso = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirCurso(curso);
    } else {
      this.atualizarCurso(curso);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do Curso</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Curso' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
                      </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Descrição</th>
            <th>Prioridade</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.curso.map((curso) => (
            <tr key={curso.id}>
              <td>{curso.nome}</td>
              <td>{curso.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(curso.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirCurso(curso.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar curso</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}