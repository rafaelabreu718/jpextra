import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';

export class Professor extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      professor: [],
      modalAberta: false
    };

    this.buscarProfessor = this.buscarProfessor.bind(this);
    this.buscarProfessor = this.buscarProfessor.bind(this);
    this.inserirProfessor = this.inserirProfessor.bind(this);
    this.atualizarProfessor = this.atualizarProfessor.bind(this);
    this.excluirProfessor = this.excluirProfessor.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarProfessor();
  }

  // GET (todos Professor)
  buscarProfessor() {
    fetch('https://localhost:44362/api/Professor')
      .then(response => response.json())
      .then(data => this.setState({ professor: data }));
  }
  
  //GET (Professor com determinado id)
  buscarProfessor(id) {
    fetch('https://localhost:44362/api/professor/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirProfessor = (professor) => {
    fetch('https://localhost:44362/api/professor', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(professor)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarProfessor();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarProfessor(professor) {
    fetch('https://localhost:44362/api/Professor/' + professor.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(professor)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarProfessor();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirProfessor = (id) => {
    fetch('https://localhost:44362/api/Professor/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarProfessor();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }
  atualizaDisciplina(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarProfessor(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const professor = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirProfessor(professor);
    } else {
      this.atualizarProfessor(professor);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do professor</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Professor' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Disciplina</Form.Label>
              <Form.Control type='text' placeholder='Disciplina' value={this.state.nome} onChange={this.atualizaDisciplina} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Descrição</th>
            <th>Prioridade</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.professor.map((professor) => (
            <tr key={professor.id}>
              <td>{professor.nome}</td>
              <td>{professor.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(professor.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirProfessor(professor.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar professor</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}